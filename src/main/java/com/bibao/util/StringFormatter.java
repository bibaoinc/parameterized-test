package com.bibao.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringFormatter {
	public static String formatNumber(String numberString) {
		DecimalFormat df = new DecimalFormat("###,###.00");
		double number = Double.parseDouble(numberString);
		number = Math.round(number * 100) / 100.0;
		return df.format(number);
	}
	
	public static String[] parseCsvLine(String line) {
		List<String> tokens = new ArrayList<>();
		parse(line.trim(), tokens);
		return tokens.stream().toArray(String[]::new);
	}
	
	private static void parse(String line, List<String> tokens) {
		int index = line.indexOf("\"");
		if (index<0) {
			String[] elements = line.split(",");
			tokens.addAll(Arrays.asList(elements));
		} else {
			int endIndex = line.indexOf("\"", index+1);
			String[] elements = line.substring(0, index).split(",");
			tokens.addAll(Arrays.asList(elements));
			tokens.add(line.substring(index + 1, endIndex));
			if (endIndex<line.length()-1) {
				parse(line.substring(endIndex+1), tokens);
			}
		}
	}
}
