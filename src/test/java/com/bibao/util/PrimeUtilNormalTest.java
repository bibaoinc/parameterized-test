package com.bibao.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class PrimeUtilNormalTest {

	@Test
	public void testIsPrime() {
		assertFalse(PrimeUtil.isPrime(-5));
		assertFalse(PrimeUtil.isPrime(0));
		assertFalse(PrimeUtil.isPrime(1));
		assertFalse(PrimeUtil.isPrime(4));
		assertTrue(PrimeUtil.isPrime(2));
		assertTrue(PrimeUtil.isPrime(5));
		assertTrue(PrimeUtil.isPrime(47));
	}

}
