package com.bibao.util;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class PrimeUtilTest {
	private int n;
	private boolean b;
	
	public PrimeUtilTest(int n, boolean b) {
		this.n = n;
		this.b = b;
	}
	
	@Parameters(name = "{index}: isPrime({0}) : {1}")
    public static Collection<Object[]> dataFromCsv() {
        return Arrays.asList(new Object[][] {
        	{-5, false},
        	{2, true},
        	{1, false},
        	{19, true},
        	{91, false},
        	{0, false},
        	{4, false},
        	{47, true}
        });
    }

    @Test
    public void testIsPrime() {
    	assertEquals(PrimeUtil.isPrime(n), b);
    }
}
