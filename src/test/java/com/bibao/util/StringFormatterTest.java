package com.bibao.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class StringFormatterTest {
	@Parameter(0)
	public String inputNumber;
	@Parameter(1)
	public String formatNumber;
	
	@Parameters(name = "{index}: {0} --> {1}")
    public static Collection<Object[]> dataFromCsv() {
        List<String> lines = FileUtils.readFromCsvFile("number-examples.csv");
        Collection<Object[]> data = new ArrayList<>();
        lines.forEach(elem -> {
        	String[] tokens = StringFormatter.parseCsvLine(elem);
        	String inputNumber = tokens[1];
        	String formatNumber = tokens[2];
        	data.add(new Object[] {inputNumber, formatNumber});
        });
        return data;
    }
    
    @Test
    public void testFormatNumber() {
    	assertEquals(formatNumber, StringFormatter.formatNumber(inputNumber));
    }
}
